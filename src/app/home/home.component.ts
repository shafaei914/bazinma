import { Component, OnInit } from '@angular/core';
import { ProjectService } from '../projects/services/project.service';
import { Project } from '../Model/project.models';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  projectList: Project[]

  constructor(private projectservice: ProjectService) { }

  ngOnInit(): void {
    this.getProduct();
  }
  getProduct() {
    this.projectservice.getProjectList().subscribe((data) => {
      this.projectList = data;
    });
  }
}
