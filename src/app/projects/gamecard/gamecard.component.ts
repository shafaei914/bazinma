import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Project } from '../../Model/project.models';

@Component({
  selector: 'app-gamecard',
  templateUrl: './gamecard.component.html',
  styleUrls: ['./gamecard.component.scss']
})
export class GamecardComponent implements OnInit {
  @Input()
  project: Project;
  avatar: string;

  public imageSrc: string;
  public progressWidth: string;

  constructor(public router: Router) { }

  ngOnInit(): void {
    this.getAvatar()
  }
  getAvatar() {
    this.avatar = 'https://api.vasapi.click/' + this.project.feature_avatar.hdpi;
  }

}
