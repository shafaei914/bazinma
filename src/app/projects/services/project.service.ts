import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class ProjectService {

  constructor(private http: HttpClient) { }

  getProjectList(): Observable<any> {
    let url = `https://api.vasapi.click/listproducts/233?limit=6&offset=0`;
    return this.http.get(url);
  }

}