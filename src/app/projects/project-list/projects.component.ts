import { Component, OnInit } from '@angular/core';
import { ProjectService } from '../services/project.service';
import { Project } from '../../Model/project.models';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss']
})
export class ProjectsComponent implements OnInit {

  projectList: Project[]

  constructor( private projectservice: ProjectService) { }

  ngOnInit(): void {
    this.getProduct();
  }

  getProduct(){
    this.projectservice.getProjectList().subscribe((data) => {
      this.projectList = data;
    });
  } 
}
