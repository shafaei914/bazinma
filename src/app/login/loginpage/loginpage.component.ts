import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { LoginService } from "../login.service";


@Component({
    selector: 'app-loginpage',
    templateUrl: './loginpage.component.html',
    styleUrls: ['./loginpage.component.scss']
})

export class LoginpageComponent implements OnInit {
    registerForm: FormGroup;
    acceptForm: FormGroup;
    public acceptPhone: boolean = false;
    public register: boolean = true; 

    constructor(private loginService: LoginService) { }

    ngOnInit(): void {
        this.initForm();
        this.initAcceptForm();
    }

    initForm() {
        this.registerForm = new FormGroup({
            mobile: new FormControl('')
        })
    }

    initAcceptForm() {
        this.acceptForm = new FormGroup({
            mobile: new FormControl(''),
            verification_code: new FormControl(''),
            nickname: new FormControl('')
        })
    }

    login() {
        const data = {
            mobile: this.registerForm.get('mobile').value,
            device_id: "browser",
            device_model: "web",
            device_os: "angularJS"
        }
        this.loginService.postMobileNumber(data).subscribe(data => {
            this.register = false;
            this.acceptPhone = true;
            console.log(data)
        });
    }

    accept() {
        const data = {
            mobile: this.acceptForm.get('mobile').value,
            nickname: this.acceptForm.get('nickname').value,
            verification_code: this.acceptForm.get('code').value,
            device_id: "browser",
            device_model: "web",
            device_os: "angularJS"
        }
        this.loginService.getVerifyCode(data).subscribe(response => {
            localStorage.setItem("token", JSON.stringify(response));
            location.reload();
        });
    }
}