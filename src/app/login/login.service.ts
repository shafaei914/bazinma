import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) { }

  postMobileNumber(mobileNumber): Observable<any> {
    let url = "https://api.vasapi.click/mobile_login_step1/5";
    return this.http.post(url,mobileNumber);
  }

  getVerifyCode(verifyCode): Observable<any> {
    let url = "https://api.vasapi.click/mobile_login_step2/5";
    return this.http.post(url,verifyCode);
  }

}
