import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { LoginpageComponent } from 'src/app/login/loginpage/loginpage.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {
  
  constructor(public dialog: MatDialog) {}

  ngOnInit(){
    console.log('hello')
  }


  openDialog(): void {
    const dialogRef = this.dialog.open(LoginpageComponent);
  }
}