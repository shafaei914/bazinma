import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FaqComponent } from './headercomponents/faq/faq.component';
import { ProjectsComponent } from './projects/project-list/projects.component';
import { GameFundersComponent } from './headercomponents/game-funders/game-funders.component';
import { HomeComponent } from './home/home.component'

const routes: Routes = [
  { path: '', component:HomeComponent},
  { path: 'faq', component: FaqComponent},
  { path: 'projects', component: ProjectsComponent},
  { path: 'game-funders', component: GameFundersComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
