import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { HeaderComponent } from './headercomponents/headers/header.component';
import { LoginpageComponent } from './login/loginpage/loginpage.component';
import { ProjectsComponent } from './projects/project-list/projects.component';
import { FaqComponent } from './headercomponents/faq/faq.component';
import { GameFundersComponent } from './headercomponents/game-funders/game-funders.component';
import { HomeComponent } from './home/home.component';
import { FooterComponent } from './footer/footer.component';

import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input'; 
import { MatFormFieldModule } from '@angular/material/form-field';
import { GamecardComponent } from './projects/gamecard/gamecard.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ProjectsComponent,
    FaqComponent,
    GameFundersComponent,
    HomeComponent,
    FooterComponent,
    LoginpageComponent,
    GamecardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatDialogModule,
    FormsModule,
    MatInputModule,
    MatFormFieldModule
  ],

  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule { }
